﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MaherARDIA.Database;

namespace MaherARDIA
{
    public partial class Administrator : Form
    {

        List <string> familleList = new List<string>() { "CNH", "ACTIWAY", "ACTIMUX", "SCANIA", "Autre" };
        bool RTB_CommHint = true;

        public Administrator()
        {
            InitializeComponent();
        }

        private void Administrator_Load(object sender, EventArgs e)
        {
            fillTableProduit();
            clearEntries();
            hintRichTextBoxComment();

        }

        private void fillTableProduit(string filter = "")
        {
            Database db = new Database();
            List<Produit> listProduit = new List<Produit>();
            listProduit = db.selectProduit(filter);
            DGV_produit.Rows.Clear();

            foreach (var row in listProduit)
            {
                DGV_produit.Rows.Add(
                        row.famille.ToString(),
                        row.produit.ToString(),
                        row.cables.ToString(),
                        row.charges.ToString(),
                        row.valise_de_test.ToString(),
                        row.plan_de_qualif == Produit.Plan_de_qualif.DISPONIBLE ? "disponible" : row.plan_de_qualif == Produit.Plan_de_qualif.NON_DISPONIBLE ? "indisponible" : "",
                        row.mode_operatoire == Produit.Mode_operatoire.FAIT ? "fait" : row.mode_operatoire == Produit.Mode_operatoire.NON_FAIT ? "non fait" : "",
                        row.emp_manip == Produit.Emp_manip.ANNXEXE ? "annexe" : row.emp_manip == Produit.Emp_manip.LABO_ARDIA ? "ardia" : "",
                        row.date_dern_utilisation.ToString(),
                        row.commentaires.ToString()
                    );
            }
        }



        private void clearEntries()
        {
            TX_cables.Text = TX_charges.Text = TX_produit.Text = TX_valise.Text = RTB_comment.Text = "";
            CB_famille.SelectedIndex = -1;
            RB_nonDisponible.Checked = RB_nonFait.Checked = RB_ardia.Checked = true;
            DGV_produit.ClearSelection();
            RTB_CommHint = true;
        }

        private bool inputControle()
        {
            if (CB_famille.Text == string.Empty) { showMsgBoxError("veillez choisir la famille"); return false; }
            if (TX_produit.Text == string.Empty) { showMsgBoxError("veillez choisir le produit"); return false; }
            if (TX_cables.Text == string.Empty) { showMsgBoxError("veillez choisir le cable"); return false; }
            if (TX_charges.Text == string.Empty) { showMsgBoxError("veillez choisir la charge"); return false; }
            if (TX_valise.Text == string.Empty) { showMsgBoxError("veillez choisir la valise"); return false; }
            return true;
        }

        private void showMsgBoxError(string msg)
        {
            MessageBox.Show(msg, "Erruer", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void PB_ajouterProduit_Click(object sender, EventArgs e)
        {
            if (!inputControle())
                return;
            Database db = new Database();

            if (!db.checkProduitExistance(TX_produit.Text.ToString().Trim()))
            {
                showMsgBoxError("produit deja existe!");
                return;
            }

            Produit produit = new Produit();

            produit.produit = TX_produit.Text.ToString();
            produit.cables = TX_cables.Text.ToString();
            produit.famille = CB_famille.Text.ToString();
            produit.charges = TX_charges.Text.ToString();
            produit.valise_de_test = TX_valise.Text.ToString();
            produit.plan_de_qualif = (RB_disponible.Checked) ? Produit.Plan_de_qualif.DISPONIBLE : Produit.Plan_de_qualif.NON_DISPONIBLE;
            produit.mode_operatoire = (RB_fait.Checked) ? Produit.Mode_operatoire.FAIT : Produit.Mode_operatoire.NON_FAIT;
            produit.emp_manip = (RB_annexe.Checked) ? Produit.Emp_manip.ANNXEXE : Produit.Emp_manip.LABO_ARDIA;
            produit.date_dern_utilisation = DP_dern_utilisation.Value.Date.ToString("yyyy/MM/dd");
            produit.commentaires = (RTB_CommHint)?"":RTB_comment.Text.ToString();

            db.addProduit(produit);
            fillTableProduit();
            clearEntries();
        }

        private void PB_modifierProduit_Click(object sender, EventArgs e)
        {
            if (!inputControle())
                return;

            int selectedIndexRow = DGV_produit.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = DGV_produit.Rows[selectedIndexRow];
            string idProduit = selectedRow.Cells["produit"].Value.ToString();

            Database db = new Database();
            Produit produit = new Produit();

            produit.produit = TX_produit.Text.ToString();
            produit.cables = TX_cables.Text.ToString();
            produit.famille = CB_famille.Text.ToString();
            produit.charges = TX_charges.Text.ToString();
            produit.valise_de_test = TX_valise.Text.ToString();
            produit.plan_de_qualif = (RB_disponible.Checked) ? Produit.Plan_de_qualif.DISPONIBLE : Produit.Plan_de_qualif.NON_DISPONIBLE;
            produit.mode_operatoire = (RB_fait.Checked) ? Produit.Mode_operatoire.FAIT : Produit.Mode_operatoire.NON_FAIT;
            produit.emp_manip = (RB_annexe.Checked) ? Produit.Emp_manip.ANNXEXE : Produit.Emp_manip.LABO_ARDIA;
            produit.date_dern_utilisation = DP_dern_utilisation.Value.Date.ToString("yyyy/MM/dd");
            produit.commentaires = (RTB_CommHint) ? "" : RTB_comment.Text.ToString();

            db.editProduit(idProduit, produit);
            fillTableProduit();
            clearEntries();
        }

        private void PB_suppProduit_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> idProduits = new List<string>();
                Database db = new Database();

                foreach (DataGridViewRow row in DGV_produit.SelectedRows)
                {
                    idProduits.Add(row.Cells["produit"].Value.ToString());
                }

                db.deleteProduit(idProduits);
                fillTableProduit();
                clearEntries();
            }
            catch (Exception) { }
        }

        private void BT_clearEntries_Click(object sender, EventArgs e)
        {
            clearEntries();
        }

        private void PB_logout_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void DGV_produit_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (DGV_produit.SelectedCells.Count > 0)
                {
                    int selectedIndexRow = DGV_produit.SelectedCells[0].RowIndex;
                    DataGridViewRow selectedRow = DGV_produit.Rows[selectedIndexRow];
                    CB_famille.SelectedIndex = familleList.IndexOf(selectedRow.Cells["famille"].Value.ToString());
                    TX_produit.Text = selectedRow.Cells["produit"].Value.ToString();
                    TX_cables.Text = selectedRow.Cells["cables"].Value.ToString();
                    TX_charges.Text = selectedRow.Cells["charges"].Value.ToString();
                    TX_valise.Text = selectedRow.Cells["valise_de_test"].Value.ToString();

                    RB_disponible.Checked = selectedRow.Cells["plan_de_qualif"].Value.ToString() == "disponible";
                    RB_nonDisponible.Checked = !RB_disponible.Checked;
                    RB_fait.Checked = selectedRow.Cells["mode_operatoire"].Value.ToString() == "fait";
                    RB_nonFait.Checked = !RB_fait.Checked;
                    RB_annexe.Checked = selectedRow.Cells["emp_manip"].Value.ToString() == "annexe";
                    RB_annexe.Checked = !RB_ardia.Checked;


                    if (selectedRow.Cells["commentaires"].Value.ToString() == "")
                    {
                        RTB_CommHint = true;
                        hintRichTextBoxComment();
                    }
                    else
                    {
                        RTB_CommHint = false;
                        RTB_comment.Text = selectedRow.Cells["commentaires"].Value.ToString();
                    }
                    //DP_dern_utilisation.Value = DateTime.ParseExact(
                    //                                    selectedRow.Cells["dern_utilisation"].Value.ToString().Trim()
                    //                                     , "yyyy-MM-dd",null);
                }

            }
            catch (Exception) { clearEntries(); }

        }

        private void TX_rechercheProduit_TextChanged(object sender, EventArgs e)
        {
            fillTableProduit(TX_rechercheProduit.Text);
        
        }

        private void Administrator_SizeChanged(object sender, EventArgs e)
        {
            PB_modifierProduit.Location = new Point((Panel_buttons.Size.Width - PB_modifierProduit.Size.Width) / 2, PB_modifierProduit.Location.Y);
            Panel_search.Location = new Point((Panel_buttons.Size.Width - Panel_search.Size.Width) / 2, Panel_search.Location.Y);
        }

        private void TX_rechercheProduit_TextChanged_1(object sender, EventArgs e)
        {
            fillTableProduit(TX_rechercheProduit.Text);
        }

        private void hintRichTextBoxComment()
        {
            if(RTB_CommHint)
            {
                RTB_comment.Text = "  ecrire votre commentaire...";
                RTB_comment.ForeColor = Color.Gray;
            }
            else
            {
                RTB_comment.ForeColor = Color.Black;

            }

        }

        private void RTB_comment_TextChanged(object sender, EventArgs e)
        {
            hintRichTextBoxComment();
        }



        private void RTB_comment_MouseDown(object sender, MouseEventArgs e)
        {
            if (RTB_comment.Text == "  ecrire votre commentaire...")
            {
                RTB_CommHint = false;
                RTB_comment.Text = "";
            }
            else if (RTB_comment.Text == "")
            {
                RTB_CommHint = true;
            }
            hintRichTextBoxComment();
        }

        private void RTB_comment_Leave(object sender, EventArgs e)
        {
            if (RTB_comment.Text == "")
            {
                RTB_CommHint = true;

            }

            hintRichTextBoxComment();

        }
    }
}
