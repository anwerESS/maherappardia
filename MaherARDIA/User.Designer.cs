﻿using System.Drawing;

namespace MaherARDIA
{
    partial class User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Panel_buttons = new System.Windows.Forms.Panel();
            this.PB_print = new System.Windows.Forms.PictureBox();
            this.PB_PDF = new System.Windows.Forms.PictureBox();
            this.PB_excel = new System.Windows.Forms.PictureBox();
            this.Panel_datagridview = new System.Windows.Forms.Panel();
            this.DGV_produit = new System.Windows.Forms.DataGridView();
            this.famille = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.produit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cables = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charges = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valise_de_test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plan_de_qualif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mode_operatoire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emp_manip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dern_utilisation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentaires = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panel_search = new System.Windows.Forms.Panel();
            this.PB_logout = new System.Windows.Forms.PictureBox();
            this.TX_rechercheProduit = new System.Windows.Forms.TextBox();
            this.PB_search = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.Panel_buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_print)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_excel)).BeginInit();
            this.Panel_datagridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_produit)).BeginInit();
            this.Panel_search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_logout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_search)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.Panel_buttons, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Panel_datagridview, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Panel_search, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 689);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Panel_buttons
            // 
            this.Panel_buttons.AllowDrop = true;
            this.Panel_buttons.AutoSize = true;
            this.Panel_buttons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Panel_buttons.Controls.Add(this.PB_print);
            this.Panel_buttons.Controls.Add(this.PB_PDF);
            this.Panel_buttons.Controls.Add(this.PB_excel);
            this.Panel_buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_buttons.Location = new System.Drawing.Point(8, 521);
            this.Panel_buttons.Margin = new System.Windows.Forms.Padding(8);
            this.Panel_buttons.Name = "Panel_buttons";
            this.Panel_buttons.Size = new System.Drawing.Size(1000, 160);
            this.Panel_buttons.TabIndex = 4;
            this.Panel_buttons.SizeChanged += new System.EventHandler(this.Panel_buttons_SizeChanged);
            // 
            // PB_print
            // 
            this.PB_print.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PB_print.Image = global::MaherARDIA.Properties.Resources.print;
            this.PB_print.Location = new System.Drawing.Point(763, 20);
            this.PB_print.Name = "PB_print";
            this.PB_print.Size = new System.Drawing.Size(114, 113);
            this.PB_print.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_print.TabIndex = 4;
            this.PB_print.TabStop = false;
            this.PB_print.Click += new System.EventHandler(this.PB_print_Click);
            // 
            // PB_PDF
            // 
            this.PB_PDF.Image = global::MaherARDIA.Properties.Resources.pdf;
            this.PB_PDF.Location = new System.Drawing.Point(458, 20);
            this.PB_PDF.Name = "PB_PDF";
            this.PB_PDF.Size = new System.Drawing.Size(114, 113);
            this.PB_PDF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_PDF.TabIndex = 3;
            this.PB_PDF.TabStop = false;
            this.PB_PDF.Click += new System.EventHandler(this.PB_PDF_Click);
            // 
            // PB_excel
            // 
            this.PB_excel.Image = global::MaherARDIA.Properties.Resources.excel;
            this.PB_excel.Location = new System.Drawing.Point(118, 22);
            this.PB_excel.Name = "PB_excel";
            this.PB_excel.Size = new System.Drawing.Size(114, 113);
            this.PB_excel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_excel.TabIndex = 2;
            this.PB_excel.TabStop = false;
            this.PB_excel.Click += new System.EventHandler(this.PB_excel_Click);
            // 
            // Panel_datagridview
            // 
            this.Panel_datagridview.Controls.Add(this.DGV_produit);
            this.Panel_datagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_datagridview.Location = new System.Drawing.Point(8, 61);
            this.Panel_datagridview.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this.Panel_datagridview.Name = "Panel_datagridview";
            this.Panel_datagridview.Size = new System.Drawing.Size(1000, 452);
            this.Panel_datagridview.TabIndex = 3;
            this.Panel_datagridview.SizeChanged += new System.EventHandler(this.Panel_datagridview_SizeChanged);
            // 
            // DGV_produit
            // 
            this.DGV_produit.AllowDrop = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.DGV_produit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.DGV_produit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGV_produit.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.DGV_produit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Cyan;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_produit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV_produit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.famille,
            this.produit,
            this.cables,
            this.charges,
            this.valise_de_test,
            this.plan_de_qualif,
            this.mode_operatoire,
            this.emp_manip,
            this.dern_utilisation,
            this.commentaires});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV_produit.DefaultCellStyle = dataGridViewCellStyle6;
            this.DGV_produit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV_produit.Location = new System.Drawing.Point(0, 0);
            this.DGV_produit.Margin = new System.Windows.Forms.Padding(8);
            this.DGV_produit.Name = "DGV_produit";
            this.DGV_produit.ReadOnly = true;
            this.DGV_produit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_produit.Size = new System.Drawing.Size(1000, 452);
            this.DGV_produit.TabIndex = 0;
            // 
            // famille
            // 
            this.famille.Frozen = true;
            this.famille.HeaderText = "Famille";
            this.famille.Name = "famille";
            this.famille.ReadOnly = true;
            this.famille.Width = 73;
            // 
            // produit
            // 
            this.produit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.produit.Frozen = true;
            this.produit.HeaderText = "Produit";
            this.produit.Name = "produit";
            this.produit.ReadOnly = true;
            this.produit.Width = 63;
            // 
            // cables
            // 
            this.cables.Frozen = true;
            this.cables.HeaderText = "Cables";
            this.cables.Name = "cables";
            this.cables.ReadOnly = true;
            this.cables.Width = 65;
            // 
            // charges
            // 
            this.charges.Frozen = true;
            this.charges.HeaderText = "Charges";
            this.charges.Name = "charges";
            this.charges.ReadOnly = true;
            this.charges.Width = 73;
            // 
            // valise_de_test
            // 
            this.valise_de_test.Frozen = true;
            this.valise_de_test.HeaderText = "Valise de test";
            this.valise_de_test.Name = "valise_de_test";
            this.valise_de_test.ReadOnly = true;
            this.valise_de_test.Width = 102;
            // 
            // plan_de_qualif
            // 
            this.plan_de_qualif.HeaderText = "Plan de qualif";
            this.plan_de_qualif.Name = "plan_de_qualif";
            this.plan_de_qualif.ReadOnly = true;
            this.plan_de_qualif.Width = 108;
            // 
            // mode_operatoire
            // 
            this.mode_operatoire.HeaderText = "Mode operatoire";
            this.mode_operatoire.Name = "mode_operatoire";
            this.mode_operatoire.ReadOnly = true;
            this.mode_operatoire.Width = 116;
            // 
            // emp_manip
            // 
            this.emp_manip.HeaderText = "Emp manip";
            this.emp_manip.Name = "emp_manip";
            this.emp_manip.ReadOnly = true;
            this.emp_manip.Width = 94;
            // 
            // dern_utilisation
            // 
            this.dern_utilisation.HeaderText = "Derniere utilisation";
            this.dern_utilisation.Name = "dern_utilisation";
            this.dern_utilisation.ReadOnly = true;
            this.dern_utilisation.Width = 136;
            // 
            // commentaires
            // 
            this.commentaires.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentaires.HeaderText = "Commentaires";
            this.commentaires.Name = "commentaires";
            this.commentaires.ReadOnly = true;
            this.commentaires.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Panel_search
            // 
            this.Panel_search.Controls.Add(this.PB_logout);
            this.Panel_search.Controls.Add(this.TX_rechercheProduit);
            this.Panel_search.Controls.Add(this.PB_search);
            this.Panel_search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_search.Location = new System.Drawing.Point(3, 3);
            this.Panel_search.Name = "Panel_search";
            this.Panel_search.Size = new System.Drawing.Size(1010, 47);
            this.Panel_search.TabIndex = 9;
            // 
            // PB_logout
            // 
            this.PB_logout.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PB_logout.Image = global::MaherARDIA.Properties.Resources.logout;
            this.PB_logout.Location = new System.Drawing.Point(956, -1);
            this.PB_logout.Name = "PB_logout";
            this.PB_logout.Size = new System.Drawing.Size(45, 45);
            this.PB_logout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_logout.TabIndex = 7;
            this.PB_logout.TabStop = false;
            this.PB_logout.Click += new System.EventHandler(this.PB_logout_Click);
            // 
            // TX_rechercheProduit
            // 
            this.TX_rechercheProduit.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_rechercheProduit.Location = new System.Drawing.Point(415, 15);
            this.TX_rechercheProduit.Name = "TX_rechercheProduit";
            this.TX_rechercheProduit.Size = new System.Drawing.Size(255, 25);
            this.TX_rechercheProduit.TabIndex = 1;
            this.TX_rechercheProduit.TextChanged += new System.EventHandler(this.TX_rechercheProduit_TextChanged_1);
            // 
            // PB_search
            // 
            this.PB_search.Image = global::MaherARDIA.Properties.Resources.search;
            this.PB_search.Location = new System.Drawing.Point(324, 1);
            this.PB_search.Name = "PB_search";
            this.PB_search.Size = new System.Drawing.Size(55, 45);
            this.PB_search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_search.TabIndex = 5;
            this.PB_search.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.ClientSize = new System.Drawing.Size(1016, 689);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(1024, 720);
            this.Name = "User";
            this.Text = " U S E R";
            this.Load += new System.EventHandler(this.User_Load);
            this.SizeChanged += new System.EventHandler(this.User_SizeChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.Panel_buttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PB_print)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_excel)).EndInit();
            this.Panel_datagridview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_produit)).EndInit();
            this.Panel_search.ResumeLayout(false);
            this.Panel_search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_logout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_search)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel Panel_datagridview;
        private System.Windows.Forms.DataGridView DGV_produit;
        private System.Windows.Forms.Panel Panel_buttons;
        private System.Windows.Forms.PictureBox PB_print;
        private System.Windows.Forms.PictureBox PB_PDF;
        private System.Windows.Forms.PictureBox PB_excel;
        private System.Windows.Forms.Panel Panel_search;
        private System.Windows.Forms.TextBox TX_rechercheProduit;
        private System.Windows.Forms.PictureBox PB_search;
        private System.Windows.Forms.PictureBox PB_logout;
        private System.Windows.Forms.DataGridViewTextBoxColumn famille;
        private System.Windows.Forms.DataGridViewTextBoxColumn produit;
        private System.Windows.Forms.DataGridViewTextBoxColumn cables;
        private System.Windows.Forms.DataGridViewTextBoxColumn charges;
        private System.Windows.Forms.DataGridViewTextBoxColumn valise_de_test;
        private System.Windows.Forms.DataGridViewTextBoxColumn plan_de_qualif;
        private System.Windows.Forms.DataGridViewTextBoxColumn mode_operatoire;
        private System.Windows.Forms.DataGridViewTextBoxColumn emp_manip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dern_utilisation;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentaires;
        private System.Windows.Forms.Timer timer1;
    }
}