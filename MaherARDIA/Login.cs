﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MaherARDIA.Database;

namespace MaherARDIA
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }


        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void BT_conneter_Click(object sender, EventArgs e)
        {
            string user = TX_utilisateur.Text.ToString();
            string pass = TX_motDePasse.Text.ToString();

            Database db = new Database();
            switch(db.Login(user, pass))
            {
                case UserType.DENIED :
                    MessageBox.Show("utilisateur ou mot de passe incorrecte", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case UserType.ADMINISTRATOR :
                    Hide();
                    new Administrator().ShowDialog();
                    Show();
                    break;
                case UserType.USER:
                    Hide();
                    new User().ShowDialog();
                    Show();
                    break;
            }

            clearTextBoxes();

        }

        private void clearTextBoxes()
        {
            TX_utilisateur.Text = TX_motDePasse.Text =  "";
        }
    }
}
