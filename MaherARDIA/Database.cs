﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace MaherARDIA
{
    class Database
    {


        const string user = "root";
        const string password = "";
        const string server = "localhost";
        const string database = "produitdb";

        public enum UserType
        {
            DENIED,
            ADMINISTRATOR,
            USER
        };


        MySqlConnection cn;
        MySqlCommand cm;
        MySqlDataReader rd;






        public struct Produit
        {
            public enum Plan_de_qualif
            {
                DISPONIBLE = 1,
                NON_DISPONIBLE = 2
            }
            public enum Mode_operatoire
            {
                FAIT = 1,
                NON_FAIT = 2
            }
            public enum Emp_manip
            {
                ANNXEXE = 1,
                LABO_ARDIA = 2
            }


            public string produit;
            public string cables;
            public string famille;
            public string charges;
            public string valise_de_test;
            public Plan_de_qualif plan_de_qualif;
            public Mode_operatoire mode_operatoire;
            public Emp_manip emp_manip;
            public string date_dern_utilisation;
            public string commentaires;
        }


        public Database()
        {
            cn = new MySqlConnection(" user = root  ; server = localhost ; database = produitdb");
        }


        public UserType Login(string user, string pass)
        {
            UserType userType;
            openDB();
            cm = new MySqlCommand("select id from login where username  = @u and password = @p", cn);
            cm.Parameters.AddWithValue("@u", user);
            cm.Parameters.AddWithValue("@p", pass);
            cm.ExecuteNonQuery();
            rd = cm.ExecuteReader();
            rd.Read();

            try
            {
                string userTypeStr = rd["id"].ToString();
                userType = (UserType)Convert.ToInt16(userTypeStr);
            }
            catch (Exception)
            {
                userType = UserType.DENIED;
            }
            finally
            {
                closeDB();
            }

            return userType;

        }

        public void addProduit(Produit produit)
        {
            openDB();
            cm = new MySqlCommand("insert into produit values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10)", cn);
            cm.Parameters.AddWithValue("@1", produit.produit);
            cm.Parameters.AddWithValue("@2", produit.cables);
            cm.Parameters.AddWithValue("@3", produit.famille);
            cm.Parameters.AddWithValue("@4", produit.charges);
            cm.Parameters.AddWithValue("@5", produit.valise_de_test);
            cm.Parameters.AddWithValue("@6", produit.plan_de_qualif);
            cm.Parameters.AddWithValue("@7", produit.mode_operatoire);
            cm.Parameters.AddWithValue("@8", produit.emp_manip);
            cm.Parameters.AddWithValue("@9", produit.date_dern_utilisation);
            cm.Parameters.AddWithValue("@10", produit.commentaires);
            cm.ExecuteNonQuery();
            closeDB();
        }

        public void editProduit(string idProduit, Produit produit)
        {
            openDB();
            cm = new MySqlCommand("UPDATE `produit` SET `produit`= @1,`cables`= @2,`famille`= @3,`charges`= @4,`valise_de_test`= @5, `plan_de_qualif`= @6,`mode_operatoire`= @7,`emp_manip`= @8,`date_dern_utilisation`= @9,`commentaires`= @10 WHERE `produit`= @11", cn);
            cm.Parameters.AddWithValue("@1", produit.produit);
            cm.Parameters.AddWithValue("@2", produit.cables);
            cm.Parameters.AddWithValue("@3", produit.famille);
            cm.Parameters.AddWithValue("@4", produit.charges);
            cm.Parameters.AddWithValue("@5", produit.valise_de_test);
            cm.Parameters.AddWithValue("@6", produit.plan_de_qualif);
            cm.Parameters.AddWithValue("@7", produit.mode_operatoire);
            cm.Parameters.AddWithValue("@8", produit.emp_manip);
            cm.Parameters.AddWithValue("@9", produit.date_dern_utilisation);
            cm.Parameters.AddWithValue("@10", produit.commentaires);
            cm.Parameters.AddWithValue("@11", idProduit);
            cm.ExecuteNonQuery();
            closeDB();
        }

        public void deleteProduit(List<string> idProduitList)
        {
            string query = "DELETE FROM `produit` WHERE 0 ";
            foreach (var idProduit in idProduitList)
            {
                query += "or produit = '" + idProduit + "'";
            }

            openDB();
            cm = new MySqlCommand(query, cn);
            cm.ExecuteNonQuery();
            closeDB();
        }

        public List<Produit> selectProduit(string filter = "")
        {
            List<Produit> listProduit = new List<Produit>();
            Produit produit = new Produit();
            cn = new MySqlConnection(" user = root  ; server = localhost ; database = produitdb");
            openDB();
            string query = "select * from produit ";
            if (filter != "")
            {
                query += "where(produit like '%" + filter + "%' or cables like '%" + filter + "%' or famille like '%" + filter + 
                         "%' or charges like '%" + filter + "%' or valise_de_test like '%" + filter + "%' or commentaires like '%" + filter + "%')";

            }

            cm = new MySqlCommand(query, cn);
            cm.ExecuteNonQuery();
            rd = cm.ExecuteReader();
            while (rd.Read())
            {
                produit.produit = rd["produit"].ToString();
                produit.cables = rd["cables"].ToString();
                produit.famille = rd["famille"].ToString();
                produit.charges = rd["charges"].ToString();
                produit.valise_de_test = rd["valise_de_test"].ToString();
                produit.plan_de_qualif = (Produit.Plan_de_qualif)Convert.ToInt16(rd["plan_de_qualif"].ToString());
                produit.mode_operatoire = (Produit.Mode_operatoire)Convert.ToInt16(rd["mode_operatoire"].ToString());
                produit.emp_manip = (Produit.Emp_manip)Convert.ToInt16(rd["emp_manip"].ToString());
                produit.date_dern_utilisation = (Convert.ToDateTime(rd["date_dern_utilisation"])).ToString("yyyy-mm-dd");
                produit.commentaires = rd["commentaires"].ToString();
                listProduit.Add(produit);
            }
            closeDB();
            return listProduit;
        }


        public bool checkProduitExistance(string produit)
        {
            bool ret = false;
            try
            {
                openDB();
                string query = "select * from produit where produit = '" + produit + "'";
                cm = new MySqlCommand(query, cn);
                cm.ExecuteNonQuery();
                rd = cm.ExecuteReader();
                rd.Read();
                if (rd["produit"].ToString() == produit)
                    ret = false;
            }
            catch(Exception)
            {
                ret = true;
            }
            finally
            {
                closeDB();
            }

            return ret;
        }

        private void openDB()
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();
        }

        private void closeDB()
        {
            if (cn.State == ConnectionState.Open)
                cn.Close();
        }


    }
}
