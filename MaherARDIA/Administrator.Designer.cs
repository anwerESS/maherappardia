﻿namespace MaherARDIA
{
    partial class Administrator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label9 = new System.Windows.Forms.Label();
            this.RTB_comment = new System.Windows.Forms.RichTextBox();
            this.DP_dern_utilisation = new System.Windows.Forms.DateTimePicker();
            this.panel6 = new System.Windows.Forms.Panel();
            this.RB_ardia = new System.Windows.Forms.RadioButton();
            this.RB_annexe = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.RB_nonFait = new System.Windows.Forms.RadioButton();
            this.RB_fait = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.RB_nonDisponible = new System.Windows.Forms.RadioButton();
            this.RB_disponible = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.CB_famille = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TX_valise = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TX_charges = new System.Windows.Forms.TextBox();
            this.Panel_buttons = new System.Windows.Forms.Panel();
            this.Panel_search = new System.Windows.Forms.Panel();
            this.TX_rechercheProduit = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.PB_logout = new System.Windows.Forms.PictureBox();
            this.PB_suppProduit = new System.Windows.Forms.PictureBox();
            this.PB_modifierProduit = new System.Windows.Forms.PictureBox();
            this.PB_ajouterProduit = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DGV_produit = new System.Windows.Forms.DataGridView();
            this.famille = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.produit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cables = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charges = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valise_de_test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plan_de_qualif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mode_operatoire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emp_manip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dern_utilisation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentaires = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panel_datagridview = new System.Windows.Forms.Panel();
            this.TX_cables = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TX_produit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BT_clearEntries = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.Panel_buttons.SuspendLayout();
            this.Panel_search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_logout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_suppProduit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_modifierProduit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ajouterProduit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_produit)).BeginInit();
            this.Panel_datagridview.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BT_clearEntries)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 388);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Derinere utulisation";
            // 
            // RTB_comment
            // 
            this.RTB_comment.Location = new System.Drawing.Point(19, 435);
            this.RTB_comment.Name = "RTB_comment";
            this.RTB_comment.Size = new System.Drawing.Size(298, 179);
            this.RTB_comment.TabIndex = 20;
            this.RTB_comment.Text = "";
            this.RTB_comment.TextChanged += new System.EventHandler(this.RTB_comment_TextChanged);
            this.RTB_comment.Leave += new System.EventHandler(this.RTB_comment_Leave);
            this.RTB_comment.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RTB_comment_MouseDown);
            // 
            // DP_dern_utilisation
            // 
            this.DP_dern_utilisation.Location = new System.Drawing.Point(139, 388);
            this.DP_dern_utilisation.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.DP_dern_utilisation.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.DP_dern_utilisation.Name = "DP_dern_utilisation";
            this.DP_dern_utilisation.Size = new System.Drawing.Size(178, 20);
            this.DP_dern_utilisation.TabIndex = 19;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.RB_ardia);
            this.panel6.Controls.Add(this.RB_annexe);
            this.panel6.Location = new System.Drawing.Point(119, 327);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(198, 37);
            this.panel6.TabIndex = 18;
            // 
            // RB_ardia
            // 
            this.RB_ardia.AutoSize = true;
            this.RB_ardia.Checked = true;
            this.RB_ardia.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_ardia.Location = new System.Drawing.Point(105, 12);
            this.RB_ardia.Name = "RB_ardia";
            this.RB_ardia.Size = new System.Drawing.Size(55, 19);
            this.RB_ardia.TabIndex = 1;
            this.RB_ardia.TabStop = true;
            this.RB_ardia.Text = "Ardia";
            this.RB_ardia.UseVisualStyleBackColor = true;
            // 
            // RB_annexe
            // 
            this.RB_annexe.AutoSize = true;
            this.RB_annexe.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_annexe.Location = new System.Drawing.Point(16, 10);
            this.RB_annexe.Name = "RB_annexe";
            this.RB_annexe.Size = new System.Drawing.Size(64, 19);
            this.RB_annexe.TabIndex = 0;
            this.RB_annexe.Text = "Annexe";
            this.RB_annexe.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Emplacement";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.RB_nonFait);
            this.panel5.Controls.Add(this.RB_fait);
            this.panel5.Location = new System.Drawing.Point(119, 284);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(198, 37);
            this.panel5.TabIndex = 16;
            // 
            // RB_nonFait
            // 
            this.RB_nonFait.AutoSize = true;
            this.RB_nonFait.Checked = true;
            this.RB_nonFait.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_nonFait.Location = new System.Drawing.Point(103, 12);
            this.RB_nonFait.Name = "RB_nonFait";
            this.RB_nonFait.Size = new System.Drawing.Size(70, 19);
            this.RB_nonFait.TabIndex = 1;
            this.RB_nonFait.TabStop = true;
            this.RB_nonFait.Text = "Non fait";
            this.RB_nonFait.UseVisualStyleBackColor = true;
            // 
            // RB_fait
            // 
            this.RB_fait.AutoSize = true;
            this.RB_fait.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_fait.Location = new System.Drawing.Point(15, 10);
            this.RB_fait.Name = "RB_fait";
            this.RB_fait.Size = new System.Drawing.Size(44, 19);
            this.RB_fait.TabIndex = 0;
            this.RB_fait.Text = "fait";
            this.RB_fait.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 298);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "Mode operatoire";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.RB_nonDisponible);
            this.panel4.Controls.Add(this.RB_disponible);
            this.panel4.Location = new System.Drawing.Point(119, 237);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 37);
            this.panel4.TabIndex = 14;
            // 
            // RB_nonDisponible
            // 
            this.RB_nonDisponible.AutoSize = true;
            this.RB_nonDisponible.Checked = true;
            this.RB_nonDisponible.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_nonDisponible.ForeColor = System.Drawing.Color.Black;
            this.RB_nonDisponible.Location = new System.Drawing.Point(93, 12);
            this.RB_nonDisponible.Name = "RB_nonDisponible";
            this.RB_nonDisponible.Size = new System.Drawing.Size(107, 19);
            this.RB_nonDisponible.TabIndex = 1;
            this.RB_nonDisponible.TabStop = true;
            this.RB_nonDisponible.Text = "Non Disponible";
            this.RB_nonDisponible.UseVisualStyleBackColor = true;
            // 
            // RB_disponible
            // 
            this.RB_disponible.AutoSize = true;
            this.RB_disponible.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RB_disponible.ForeColor = System.Drawing.Color.Black;
            this.RB_disponible.Location = new System.Drawing.Point(10, 12);
            this.RB_disponible.Name = "RB_disponible";
            this.RB_disponible.Size = new System.Drawing.Size(81, 19);
            this.RB_disponible.TabIndex = 0;
            this.RB_disponible.Text = "Disponible";
            this.RB_disponible.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Famille";
            // 
            // CB_famille
            // 
            this.CB_famille.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_famille.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CB_famille.FormattingEnabled = true;
            this.CB_famille.Items.AddRange(new object[] {
            "CNH",
            "ACTIWAY",
            "ACTIMUX",
            "SCANIA",
            "Autre"});
            this.CB_famille.Location = new System.Drawing.Point(120, 26);
            this.CB_famille.Name = "CB_famille";
            this.CB_famille.Size = new System.Drawing.Size(198, 24);
            this.CB_famille.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 15);
            this.label6.TabIndex = 8;
            this.label6.Text = "Plan de qualif";
            // 
            // TX_valise
            // 
            this.TX_valise.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_valise.Location = new System.Drawing.Point(120, 201);
            this.TX_valise.Name = "TX_valise";
            this.TX_valise.Size = new System.Drawing.Size(198, 21);
            this.TX_valise.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "valise de test";
            // 
            // TX_charges
            // 
            this.TX_charges.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_charges.Location = new System.Drawing.Point(120, 159);
            this.TX_charges.Name = "TX_charges";
            this.TX_charges.Size = new System.Drawing.Size(198, 21);
            this.TX_charges.TabIndex = 5;
            // 
            // Panel_buttons
            // 
            this.Panel_buttons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.Panel_buttons.Controls.Add(this.Panel_search);
            this.Panel_buttons.Controls.Add(this.PB_logout);
            this.Panel_buttons.Controls.Add(this.PB_suppProduit);
            this.Panel_buttons.Controls.Add(this.PB_modifierProduit);
            this.Panel_buttons.Controls.Add(this.PB_ajouterProduit);
            this.Panel_buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_buttons.Location = new System.Drawing.Point(365, 476);
            this.Panel_buttons.Margin = new System.Windows.Forms.Padding(0, 8, 8, 8);
            this.Panel_buttons.Name = "Panel_buttons";
            this.Panel_buttons.Size = new System.Drawing.Size(643, 205);
            this.Panel_buttons.TabIndex = 3;
            // 
            // Panel_search
            // 
            this.Panel_search.Controls.Add(this.TX_rechercheProduit);
            this.Panel_search.Controls.Add(this.pictureBox4);
            this.Panel_search.Location = new System.Drawing.Point(154, 139);
            this.Panel_search.Name = "Panel_search";
            this.Panel_search.Size = new System.Drawing.Size(301, 62);
            this.Panel_search.TabIndex = 7;
            // 
            // TX_rechercheProduit
            // 
            this.TX_rechercheProduit.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_rechercheProduit.Location = new System.Drawing.Point(100, 15);
            this.TX_rechercheProduit.Name = "TX_rechercheProduit";
            this.TX_rechercheProduit.Size = new System.Drawing.Size(198, 25);
            this.TX_rechercheProduit.TabIndex = 1;
            this.TX_rechercheProduit.TextChanged += new System.EventHandler(this.TX_rechercheProduit_TextChanged_1);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::MaherARDIA.Properties.Resources.search;
            this.pictureBox4.Location = new System.Drawing.Point(9, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(69, 52);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            // 
            // PB_logout
            // 
            this.PB_logout.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PB_logout.Image = global::MaherARDIA.Properties.Resources.logout;
            this.PB_logout.Location = new System.Drawing.Point(572, 141);
            this.PB_logout.Name = "PB_logout";
            this.PB_logout.Size = new System.Drawing.Size(67, 63);
            this.PB_logout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_logout.TabIndex = 6;
            this.PB_logout.TabStop = false;
            this.PB_logout.Click += new System.EventHandler(this.PB_logout_Click);
            // 
            // PB_suppProduit
            // 
            this.PB_suppProduit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PB_suppProduit.Image = global::MaherARDIA.Properties.Resources.Del;
            this.PB_suppProduit.Location = new System.Drawing.Point(480, 20);
            this.PB_suppProduit.Name = "PB_suppProduit";
            this.PB_suppProduit.Size = new System.Drawing.Size(114, 113);
            this.PB_suppProduit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_suppProduit.TabIndex = 4;
            this.PB_suppProduit.TabStop = false;
            this.PB_suppProduit.Click += new System.EventHandler(this.PB_suppProduit_Click);
            // 
            // PB_modifierProduit
            // 
            this.PB_modifierProduit.Image = global::MaherARDIA.Properties.Resources.edit2;
            this.PB_modifierProduit.Location = new System.Drawing.Point(266, 20);
            this.PB_modifierProduit.Name = "PB_modifierProduit";
            this.PB_modifierProduit.Size = new System.Drawing.Size(114, 113);
            this.PB_modifierProduit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_modifierProduit.TabIndex = 3;
            this.PB_modifierProduit.TabStop = false;
            this.PB_modifierProduit.Click += new System.EventHandler(this.PB_modifierProduit_Click);
            // 
            // PB_ajouterProduit
            // 
            this.PB_ajouterProduit.Image = global::MaherARDIA.Properties.Resources.add;
            this.PB_ajouterProduit.Location = new System.Drawing.Point(67, 20);
            this.PB_ajouterProduit.Name = "PB_ajouterProduit";
            this.PB_ajouterProduit.Size = new System.Drawing.Size(114, 113);
            this.PB_ajouterProduit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_ajouterProduit.TabIndex = 2;
            this.PB_ajouterProduit.TabStop = false;
            this.PB_ajouterProduit.Click += new System.EventHandler(this.PB_ajouterProduit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Charges";
            // 
            // DGV_produit
            // 
            this.DGV_produit.AllowDrop = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.DGV_produit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV_produit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_produit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV_produit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_produit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.famille,
            this.produit,
            this.cables,
            this.charges,
            this.valise_de_test,
            this.plan_de_qualif,
            this.mode_operatoire,
            this.emp_manip,
            this.dern_utilisation,
            this.commentaires});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV_produit.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGV_produit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV_produit.Location = new System.Drawing.Point(0, 0);
            this.DGV_produit.Margin = new System.Windows.Forms.Padding(0, 8, 8, 8);
            this.DGV_produit.MaximumSize = new System.Drawing.Size(1024, 720);
            this.DGV_produit.Name = "DGV_produit";
            this.DGV_produit.ReadOnly = true;
            this.DGV_produit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_produit.Size = new System.Drawing.Size(643, 452);
            this.DGV_produit.TabIndex = 0;
            this.DGV_produit.SelectionChanged += new System.EventHandler(this.DGV_produit_SelectionChanged);
            // 
            // famille
            // 
            this.famille.HeaderText = "famille";
            this.famille.Name = "famille";
            this.famille.ReadOnly = true;
            this.famille.Width = 70;
            // 
            // produit
            // 
            this.produit.HeaderText = "produit";
            this.produit.Name = "produit";
            this.produit.ReadOnly = true;
            this.produit.Width = 71;
            // 
            // cables
            // 
            this.cables.HeaderText = "cables";
            this.cables.Name = "cables";
            this.cables.ReadOnly = true;
            this.cables.Width = 63;
            // 
            // charges
            // 
            this.charges.HeaderText = "charges";
            this.charges.Name = "charges";
            this.charges.ReadOnly = true;
            this.charges.Width = 71;
            // 
            // valise_de_test
            // 
            this.valise_de_test.HeaderText = "valise de test";
            this.valise_de_test.Name = "valise_de_test";
            this.valise_de_test.ReadOnly = true;
            this.valise_de_test.Width = 76;
            // 
            // plan_de_qualif
            // 
            this.plan_de_qualif.HeaderText = "plan de qualification";
            this.plan_de_qualif.Name = "plan_de_qualif";
            this.plan_de_qualif.ReadOnly = true;
            this.plan_de_qualif.Width = 132;
            // 
            // mode_operatoire
            // 
            this.mode_operatoire.HeaderText = "mode operatoire";
            this.mode_operatoire.Name = "mode_operatoire";
            this.mode_operatoire.ReadOnly = true;
            this.mode_operatoire.Width = 105;
            // 
            // emp_manip
            // 
            this.emp_manip.HeaderText = "emplacement manip";
            this.emp_manip.Name = "emp_manip";
            this.emp_manip.ReadOnly = true;
            this.emp_manip.Width = 123;
            // 
            // dern_utilisation
            // 
            this.dern_utilisation.HeaderText = "derniere utilisation";
            this.dern_utilisation.Name = "dern_utilisation";
            this.dern_utilisation.ReadOnly = true;
            this.dern_utilisation.Width = 125;
            // 
            // commentaires
            // 
            this.commentaires.HeaderText = "commentaires";
            this.commentaires.Name = "commentaires";
            this.commentaires.ReadOnly = true;
            this.commentaires.Width = 103;
            // 
            // Panel_datagridview
            // 
            this.Panel_datagridview.Controls.Add(this.DGV_produit);
            this.Panel_datagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_datagridview.Location = new System.Drawing.Point(365, 8);
            this.Panel_datagridview.Margin = new System.Windows.Forms.Padding(0, 8, 8, 8);
            this.Panel_datagridview.Name = "Panel_datagridview";
            this.Panel_datagridview.Size = new System.Drawing.Size(643, 452);
            this.Panel_datagridview.TabIndex = 2;
            // 
            // TX_cables
            // 
            this.TX_cables.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_cables.Location = new System.Drawing.Point(119, 115);
            this.TX_cables.Name = "TX_cables";
            this.TX_cables.Size = new System.Drawing.Size(198, 21);
            this.TX_cables.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cables";
            // 
            // TX_produit
            // 
            this.TX_produit.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_produit.Location = new System.Drawing.Point(119, 73);
            this.TX_produit.Name = "TX_produit";
            this.TX_produit.Size = new System.Drawing.Size(198, 21);
            this.TX_produit.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Produit";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.panel1.Controls.Add(this.BT_clearEntries);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.RTB_comment);
            this.panel1.Controls.Add(this.DP_dern_utilisation);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.CB_famille);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.TX_valise);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.TX_charges);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.TX_cables);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.TX_produit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(8);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(349, 673);
            this.panel1.TabIndex = 1;
            // 
            // BT_clearEntries
            // 
            this.BT_clearEntries.Image = global::MaherARDIA.Properties.Resources.cross_512;
            this.BT_clearEntries.Location = new System.Drawing.Point(297, 622);
            this.BT_clearEntries.Name = "BT_clearEntries";
            this.BT_clearEntries.Size = new System.Drawing.Size(45, 45);
            this.BT_clearEntries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BT_clearEntries.TabIndex = 22;
            this.BT_clearEntries.TabStop = false;
            this.BT_clearEntries.Click += new System.EventHandler(this.BT_clearEntries_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 365F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.Panel_buttons, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Panel_datagridview, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 689);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Administrator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ClientSize = new System.Drawing.Size(1016, 689);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Administrator";
            this.Text = "A D M I N I S T R A T E U R ";
            this.Load += new System.EventHandler(this.Administrator_Load);
            this.SizeChanged += new System.EventHandler(this.Administrator_SizeChanged);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.Panel_buttons.ResumeLayout(false);
            this.Panel_search.ResumeLayout(false);
            this.Panel_search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_logout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_suppProduit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_modifierProduit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_ajouterProduit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_produit)).EndInit();
            this.Panel_datagridview.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BT_clearEntries)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox BT_clearEntries;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox RTB_comment;
        private System.Windows.Forms.DateTimePicker DP_dern_utilisation;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton RB_ardia;
        private System.Windows.Forms.RadioButton RB_annexe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton RB_nonFait;
        private System.Windows.Forms.RadioButton RB_fait;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton RB_nonDisponible;
        private System.Windows.Forms.RadioButton RB_disponible;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CB_famille;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TX_valise;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TX_charges;
        private System.Windows.Forms.Panel Panel_buttons;
        private System.Windows.Forms.PictureBox PB_logout;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox PB_suppProduit;
        private System.Windows.Forms.PictureBox PB_modifierProduit;
        private System.Windows.Forms.PictureBox PB_ajouterProduit;
        private System.Windows.Forms.TextBox TX_rechercheProduit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView DGV_produit;
        private System.Windows.Forms.Panel Panel_datagridview;
        private System.Windows.Forms.TextBox TX_cables;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TX_produit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel Panel_search;
        private System.Windows.Forms.DataGridViewTextBoxColumn famille;
        private System.Windows.Forms.DataGridViewTextBoxColumn produit;
        private System.Windows.Forms.DataGridViewTextBoxColumn cables;
        private System.Windows.Forms.DataGridViewTextBoxColumn charges;
        private System.Windows.Forms.DataGridViewTextBoxColumn valise_de_test;
        private System.Windows.Forms.DataGridViewTextBoxColumn plan_de_qualif;
        private System.Windows.Forms.DataGridViewTextBoxColumn mode_operatoire;
        private System.Windows.Forms.DataGridViewTextBoxColumn emp_manip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dern_utilisation;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentaires;
    }
}