﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MaherARDIA.Database;
using DGVPrinterHelper;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace MaherARDIA
{
    public partial class User : Form
    {
        public User()
        {
            InitializeComponent();
        }

        private void User_Load(object sender, EventArgs e)
        {
            fillTableProduit();

            //DGV_produit.Columns[9].Width = 120;
        }

        private void fillTableProduit(string filter = "")
        {
            Database db = new Database();
            List<Produit> listProduit = new List<Produit>();
            listProduit = db.selectProduit(filter);
            DGV_produit.Rows.Clear();

            foreach (var row in listProduit)
            {
                DGV_produit.Rows.Add(
                        row.famille.ToString(),
                        row.produit.ToString(),
                        row.cables.ToString(),
                        row.charges.ToString(),
                        row.valise_de_test.ToString(),
                        row.plan_de_qualif == Produit.Plan_de_qualif.DISPONIBLE ? "disponible" : row.plan_de_qualif == Produit.Plan_de_qualif.NON_DISPONIBLE ? "indisponible" : "",
                        row.mode_operatoire == Produit.Mode_operatoire.FAIT ? "fait" : row.mode_operatoire == Produit.Mode_operatoire.NON_FAIT ? "non fait" : "",
                        row.emp_manip == Produit.Emp_manip.ANNXEXE ? "annexe" : row.emp_manip == Produit.Emp_manip.LABO_ARDIA ? "ardia" : "",
                        row.date_dern_utilisation.ToString(),
                        row.commentaires.ToString()
                    );
            }
        }

        private void showMsgBoxError(string msg)
        {
            MessageBox.Show(msg, "Erruer", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Panel_buttons_SizeChanged(object sender, EventArgs e)
        {
            PB_PDF.Location = new Point((Panel_buttons.Size.Width - PB_PDF.Size.Width) / 2, PB_PDF.Location.Y);
            Panel_search.Location = new Point((Panel_buttons.Size.Width - Panel_search.Size.Width) / 2, Panel_search.Location.Y);
        }

        private void User_SizeChanged(object sender, EventArgs e)
        {
            Panel_datagridview.Size = new Size(this.Width, Panel_datagridview.Height);
            TX_rechercheProduit.Location = new Point((Panel_search.Size.Width-TX_rechercheProduit.Location.X)/2, TX_rechercheProduit.Location.Y);
            PB_search.Location = new Point(TX_rechercheProduit.Location.X - new Point((int)(1.7*PB_search.Width)).X , PB_search.Location.Y); 


        }

        private void Panel_datagridview_SizeChanged(object sender, EventArgs e)
        {
            DGV_produit.Size = new Size(Panel_datagridview.Width, DGV_produit.Height);
        }

        private void TX_rechercheProduit_TextChanged(object sender, EventArgs e)
        {
            fillTableProduit(TX_rechercheProduit.Text);

        }

        private void TX_rechercheProduit_TextChanged_1(object sender, EventArgs e)
        {
            fillTableProduit(TX_rechercheProduit.Text);
            timer1.Enabled = TX_rechercheProduit.Text == "";

        }

        private void copyAlltoClipboard()
        {
            DGV_produit.SelectAll();
            DataObject dataObj = DGV_produit.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void PB_excel_Click(object sender, EventArgs e)
        {
            //copyAlltoClipboard();
            //Microsoft.Office.Interop.Excel.Application xlexcel;
            //Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            //Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            //object misValue = System.Reflection.Missing.Value;
            //xlexcel = new Microsoft.Office.Interop.Excel.Application();
            //xlexcel.Visible = true;
            //xlWorkBook = xlexcel.Workbooks.Add(misValue);
            //xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            //CR.Select();
            //xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

            if (DGV_produit.Rows.Count > 0)
            {

                Microsoft.Office.Interop.Excel.Application xcelApp = new Microsoft.Office.Interop.Excel.Application();
                xcelApp.Application.Workbooks.Add(Type.Missing);

                for (int i = 1; i < DGV_produit.Columns.Count + 1; i++)
                {
                    xcelApp.Cells[1, i] = DGV_produit.Columns[i - 1].HeaderText;
                }

                for (int i = 0; i < DGV_produit.Rows.Count; i++)
                {
                    for (int j = 0; j < DGV_produit.Columns.Count; j++)
                    {
                        try
                        {
                            xcelApp.Cells[i + 2, j + 1] = DGV_produit.Rows[i].Cells[j].Value.ToString();

                        }
                        catch (System.NullReferenceException)
                        {
                            xcelApp.Cells[i + 2, j + 1] = "";

                        }
                    }
                }
                xcelApp.Columns.AutoFit();
                xcelApp.Visible = true;
            }

        }

        private void PB_logout_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
                fillTableProduit();
        }

        private void PB_PDF_Click(object sender, EventArgs e)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            PdfPTable pdfPTable = new PdfPTable(DGV_produit.Columns.Count);
            pdfPTable.DefaultCell.Padding = 3;
            float[] widths = new float[] { 250f, 250f, 250f, 250f, 200f, 180f, 120f, 120f, 180f, 600f };
            pdfPTable.SetWidths(widths);
            pdfPTable.WidthPercentage = 100;
            

            pdfPTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfPTable.DefaultCell.BorderWidth = 1;

            iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 6, iTextSharp.text.Font.NORMAL);
            //Add Header
            foreach(DataGridViewColumn column in DGV_produit.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase (column.HeaderText, text));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdfPTable.AddCell(cell);
            }
            //Add DataRow
            foreach (DataGridViewRow row in DGV_produit.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    try
                    {
                        pdfPTable.AddCell(new Phrase(cell.Value.ToString(), text));
                    }
                    catch (System.NullReferenceException)
                    {
                        pdfPTable.AddCell(new Phrase("", text));
                    }
                }
            }

            var savefiledialog = new SaveFileDialog();
            savefiledialog.FileName = "*";
            savefiledialog.DefaultExt = ".pdf";
            if(savefiledialog.ShowDialog() == DialogResult.OK)
            {
                using (System.IO.FileStream stream = new System.IO.FileStream(savefiledialog.FileName, System.IO.FileMode.Create))
                {
                    Document pdfdoc = new Document(PageSize.A4,10f,10f,10f,0f);
                    PdfWriter.GetInstance(pdfdoc, stream);
                    pdfdoc.Open();
                    pdfdoc.Add(pdfPTable);
                    pdfdoc.Close();
                    stream.Close();
                }
            }
        }

        private void PB_print_Click(object sender, EventArgs e)
        {
            try
            {
                DGVPrinter printer = new DGVPrinter();
                printer.Title = "produit";
                printer.SubTitle = "liste des produits ";
                printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
                printer.PageNumbers = true;
                printer.PageNumberInHeader = false;
                printer.PorportionalColumns = true;
                printer.HeaderCellAlignment = StringAlignment.Near;
                printer.Footer = "ARDIA";
                printer.FooterSpacing = 15;
                printer.PrintDataGridView(DGV_produit);
            }
            catch (Exception) { }


        }
    }
}
