﻿namespace MaherARDIA
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TX_motDePasse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TX_utilisateur = new System.Windows.Forms.TextBox();
            this.BT_conneter = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TX_motDePasse);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TX_utilisateur);
            this.groupBox1.Controls.Add(this.BT_conneter);
            this.groupBox1.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(44, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 200);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mot de passe";
            // 
            // TX_motDePasse
            // 
            this.TX_motDePasse.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_motDePasse.Location = new System.Drawing.Point(134, 77);
            this.TX_motDePasse.Name = "TX_motDePasse";
            this.TX_motDePasse.PasswordChar = '*';
            this.TX_motDePasse.Size = new System.Drawing.Size(153, 25);
            this.TX_motDePasse.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Utilisateur";
            // 
            // TX_utilisateur
            // 
            this.TX_utilisateur.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TX_utilisateur.Location = new System.Drawing.Point(134, 30);
            this.TX_utilisateur.Name = "TX_utilisateur";
            this.TX_utilisateur.Size = new System.Drawing.Size(153, 25);
            this.TX_utilisateur.TabIndex = 1;
            // 
            // BT_conneter
            // 
            this.BT_conneter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BT_conneter.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_conneter.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BT_conneter.Location = new System.Drawing.Point(103, 133);
            this.BT_conneter.Name = "BT_conneter";
            this.BT_conneter.Size = new System.Drawing.Size(136, 38);
            this.BT_conneter.TabIndex = 0;
            this.BT_conneter.Text = "Se connecter";
            this.BT_conneter.UseVisualStyleBackColor = false;
            this.BT_conneter.Click += new System.EventHandler(this.BT_conneter_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MaherARDIA.Properties.Resources.ardia2;
            this.pictureBox1.Location = new System.Drawing.Point(119, 232);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 71);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(425, 313);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Login";
            this.Text = "L O G I N";
            this.Load += new System.EventHandler(this.Login_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TX_motDePasse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TX_utilisateur;
        private System.Windows.Forms.Button BT_conneter;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

